class Date {
	public:
		int day;
		int month;
		int year;
		Date(int, int, int);
		Date();
		Date(const Date&);
		void setDate();
		void show();
		~Date();
};
