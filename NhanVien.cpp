# include "NhanVien.h"
using namespace std;

NhanVien::NhanVien(string nID, string nName, Date nDate, bool nGender, double nSalary) :
			id(nID), 
			name(nName), 
			sDate(nDate), 
			gender(nGender), 
			salary(nSalary) {
}
NhanVien::NhanVien() :
	id("1111"),
	name("Doan Xuan Hung"),
	sDate(Date(25, 01, 2000)),
	gender(true),
	salary(2000) {
}
NhanVien::NhanVien(const NhanVien& p) {
	this->id = p.id;
	this->name = p.name;
	this->sDate = p.sDate;
	this->gender = p.gender;
	this->salary = p.salary;
}

void NhanVien::setNhanVien() {
	// Khai bao ca thuoc tinh
	string nID, nName;
	Date nDate;
	bool nGender;
	double nSalary;
	int numberGender;
	// Input cac thuoc tinh
	// id
	while (true) {
		cout << "Nhap ID: ";
		cin >> nID;
		cin.ignore();
		if (nID.length() != 4) {
			cout << "ID nhap vao khong phu hop!!!\n";
			cout << "ID phai gom co 4 ky tu\n";
		}
		else {
			break;
		}
	}
	// name
	cout << "Nhap ten: ";
	getline(cin, nName);
	// gioi tinh
	cout << "Nhap gioi tinh: " << endl;
	cout << "	Nam : 1" << endl;
	cout << "	Nu: 2\n";
	cin >> numberGender;
	if (numberGender == 1) {
		nGender = true;
	}
	else {
		nGender = false;
	}
	// ngay thang
	cout << "Ngay ki hop dong: \n";
	nDate.setDate();
	// Tien luong
	cout << "Tien luong cua nhan vien: ";
	cin >> nSalary;
	cout << endl;
	// up cac thuoc tinh vao header
	this->id = nID;
	this->name = nName;
	this->sDate = nDate;
	this->salary = nSalary;
	this->gender = nGender;

}
void NhanVien::show() {
	cout << "ID: " << this->id << endl;
	cout << "Ten nhan vien: " << this->name << endl;
	cout << "Ngay ki hop dong: ";
	this->sDate.show();
	cout << endl;
	string genderString;
	if (this->gender == false) {
		genderString = "Nu";
	}
	else {
		genderString = "Nam";
	}
	cout << "Gioi tinh: " << this->gender << endl;
	cout << "Luong: " << this->salary << "$" << endl;
}

NhanVien::~NhanVien() {

}