# include "Date.cpp"

class NhanVien {
	public:
	string id;
	string name; 
	Date sDate;
	bool gender;
	double salary;
	NhanVien(string, string, Date, bool, double);
	NhanVien();
	NhanVien(const NhanVien&);
	void setNhanVien();
	void show();
	~NhanVien();
};