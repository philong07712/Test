#include "NhanVien.cpp"

class List {
	public:
		NhanVien *list;
		int length;
		//
		List(const List&);
		List();
		List(NhanVien &);	
		void show();
		void add();
		bool isEquals(string, string);
		void update();
		void Delete();
		int Search();
		void Sort(bool (int, int));
		~List();
};
