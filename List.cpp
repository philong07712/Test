#include "List.h"

List::List() {
	this->list = NULL;
	this->length = 0;
}

List::List(NhanVien& p) {
	this->list = new NhanVien(p);
	this->length = 1;
}

List::List(const List& p) {
	this->length = p.length;
	this->list = new NhanVien[this->length];
	for (int i = 0; i < this->length; i++) {
		*(this->list + i) = *(p.list + i);
	}
}

void List::show() {
	for (int i = 0; i < this->length; i++) {
		(this->list + i)->show();
		cout << endl;
	}
}


void List::add() {
	NhanVien newNhanVien;
	int k;
	cout << "Chon 0 <= k <= " << this->length << " ";
	cout << "Nhap gia tri k: \n";
	cin >> k;
	if (k < 0 || k > this->length) {
		cout << "k khong hop le!!!!\n";
		return;
	}
	newNhanVien.setNhanVien();
	NhanVien *nList = this->list;
	this->list = new NhanVien[this->length + 1];
	for (int i = 0; i < this->length; i++) {
		*(this->list + i) = *(nList + i);
	}
    for (int i = this->length; i > k; i--) {
        *(this->list + i) =  *(this->list + i - 1);
    }	

	*(this->list + k) = newNhanVien;
	this->length++;
	delete[] nList;
}
void List::update() {
	int index = this->Search();
	 if (index == -1) {
		 cout << "Tim kiem that bai, id khong ton tai!!!\n";
		 return;
	 }
	 (*(this->list + index)).setNhanVien();
}
void List::Delete() {
	int k;
	cout << "Chon 0 <= k < " << this->length << " ";
	cout << "Nhap gia tri k: \n";
	cin >> k;
	if (k < 0 || k >= this->length) {
		cout << "k khong hop le!!!!\n";
		return;
	}

	NhanVien *nList = this->list;
	this->list = new NhanVien[this->length - 1];
	for (int i = 0; i < this->length - 1; i++) {
		*(this->list + i) = *(nList + i);
		if (i >= k) {
			*(this->list + i) = *(nList + i + 1);
		}
	}

	this->length--;
	delete[] nList;

}
int List::Search() {
	string s1;
	while (true) {
		cout << "Nhap ID can tim kiem: ";
		cin >> s1;
		if (s1.length() != 4) {
			cout << "ID nhap vao khong phu hop!!!\n";
			cout << "ID phai gom co 4 ky tu\n";	\
		}
		else {
			break;
		}
	}


	for (int i = 0; i < this->length; i++) {
		string checkID = (*(this->list + i)).id;
		if (isEquals(checkID, s1)) {
			return i;
		}
	}
	return -1;
}

bool List::isEquals(string s1, string s2) {
	for (int i = 0; i < s1.length(); i++) {
		if (s1[i] != s2[i]) {
			return false;
		} 
	}
	return true;
}

void swap(NhanVien &x, NhanVien&y) {
	NhanVien temp = x;
	x = y;
	y = temp;
}

int	stringtoInt(string s) {
	int newInt = 0;
	for (int i = 0; i < s.length(); i++) {
		newInt = newInt * 10 + s[i];
	}
	return newInt;
}
bool ascending(int left, int right) {
    return left > right;
}

bool descending(int left, int right) {
    return right > left;
}

void List::Sort(bool compare(int, int)) {
	for (int i = 0; i < this->length - 1; i++) {
		for (int j = 0; j < this->length - i - 1; j++) {
			int id1 = stringtoInt((*(this->list + j)).id);
			int id2 = stringtoInt((*(this->list + j + 1)).id);
			if (compare(id1, id2)) {
				swap(*(this->list + j), *(this->list + j + 1));
			}
		}
	}
}
List::~List() {
	delete[] this->list;
}