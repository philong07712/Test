# include "Date.h"
# include <iostream>
using namespace std;

Date::Date(int x, int y, int z) : 
		day(x), 
		month(y), 
		year(z) {
}

Date::Date() : 
		day(20), 
		month(1), 
		year(2000) {
}

Date::Date(const Date& nDay) {
	this->day = nDay.day;
	this->month = nDay.month;
	this->year = nDay.year;
}
void Date::setDate() {
	int nDay, nMonth, nYear;
	cout << "Nhap ngay: ";
	cin >> nDay;
	while (true) {
		cout << "Nhap thang: ";
		cin >> nMonth;
		if (nMonth < 0 || nMonth > 12) {
			cout << "Thang khong hop le!!!" << endl;
		}
		else {
			break;
		}
	}
	while (true) {
		cout << "Nhap nam: ";
		cin >> nYear;
		if (nYear < 0) {
			cout << "Nam khong hop le!!!" << endl;
		}
		else {
			break;
		}
	}
	int maxDayInMonth;
	if (nMonth == 4 || nMonth == 6 || nMonth == 9 || nMonth == 11) {
			maxDayInMonth = 30;
		}
	else if (month == 02) {
		bool leapyear = (nYear % 4 == 0 && nYear % 100 != 0) || (nYear % 400 == 0);
		if (leapyear == 0)
				maxDayInMonth = 28;
		else 
				maxDayInMonth = 29;
	}
	else 
		maxDayInMonth = 31;
	while (true) {
		if (nDay < maxDayInMonth && nDay > 0) {
			break;
		}
		cout << "Ngay khong hop le!!!\n";
		cout << "Nhap lai ngay: ";
		cin >> nDay;
	}
	this->day = nDay;
	this->month = nMonth;
	this->year = nYear;
}
void Date::show() {
	cout << this->day << "/" << this->month << "/" << this->year; 
}
Date::~Date() {
}

