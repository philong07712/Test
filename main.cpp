# include "List.cpp"
#include <bits/stdc++.h>
using namespace std;
void printMenu() {
    cout << "\nChon ham ban muon su dung:" << endl;
    cout << "1. Ham them mot nhan vien vao vi tri k, in ra ket qua sau khi chen." << endl;
    cout << "2. Xoa mot phan tu cua danh sach tai vi tri thu k, in ra ket qua sau khi xoa." << endl;
    cout << "3. Bubble Sort va in ra." << endl;
    cout << "4. Linear Search." << endl;
    cout << "5. Cap nhat lai gia tri cua danh sach." << endl;
    cout << "6. In ra danh sach hien tai." << endl;
    cout << "7(Ctrl + C). thoat chuong trinh.\n" << endl;
    }
int main() {
    List nVList;
    int choice;
    int index;
    while (true) {
        printMenu();
        cin >> choice;
        switch (choice)
        {
        case 1:
            nVList.add();
            nVList.show();
            break;
        case 2:
            nVList.Delete();
            nVList.show();
            break;
        case 3:
            cout << "1. Sap xep tang dan\n";
            cout << "2. Sap xep giam dan";
            int k;
            cin >> k;
            if (k == 1) {
                nVList.Sort(ascending);
            }
            else {
                nVList.Sort(descending);
            }
            nVList.show();
            break;
        case 4:
            index = nVList.Search();
            if (index == -1) {
                cout << "Tim kiem that bai, id khong ton tai!!!";
            }
            else {
                cout << "vi tri cua id can tim la: " << index;
            }
            break;
        case 5:
            nVList.update();
            break;
        case 6:
            nVList.show();
        case 7:
            return 0;
        default:
            cout << "Lua chon sai!!!!";
            break;
        }
    }
}
